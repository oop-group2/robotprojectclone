/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author werapan
 */
public class TableMap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;
    private Obj[] object = new Obj[100];
    int objcount = 0;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void addObj(Obj obj) {
        object[objcount] = obj;
        objcount++;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
        addObj(robot);
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
        addObj(bomb);
    }

    public void printSymbolOn(int x, int y) {
        char symbol = '-';
        for (int o = 0; o < objcount; o++) {
            if (object[o].isOn(x, y)) {
                symbol = object[o].getSymbol();
            }
        }
        System.out.print(symbol);
    }
    public void showMap() {
        showTitle();
        System.out.println(robot);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                printSymbolOn(x,y);
            }
            showNewLine();
        }

    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showObj(Obj obj) {
        System.out.print(obj.getSymbol());
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        // x -> 0-(width-1), y -> 0-(height-1)
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
    public boolean isTree(int x, int y) {
        for (int o = 0; o < objcount; o++) {
            if (object[o] instanceof Tree && object[o].isOn(x, y)) {
                return true;
            }
        }
        return false;
    }
    public int fillFuel(int x, int y) {
        for (int o = 0; o < objcount; o++) {
            if (object[o] instanceof Fuel && object[o].isOn(x, y)) {
                Fuel fuel =(Fuel) object[o];
                return fuel.fillfuel();
            }
        }
        return 0;
    }
}
